#!/bin/env python3
import json
import uuid
import time
import random
import logging
import lorem


def main(sleeptime):

    while True:
        level = random.randint(0, 5) * 10
        levelName = logging.getLevelName(level)

        data = {
            "level": levelName,
            "message": lorem.sentence(),
            "uuid": str(uuid.uuid4())
        }

        print(json.dumps(data))
        time.sleep(sleeptime)


if __name__ == "__main__":
    main(5)
